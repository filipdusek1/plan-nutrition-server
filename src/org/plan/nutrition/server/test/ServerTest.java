package org.plan.nutrition.server.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.plan.nutrition.server.configuration.ProfileDefinition;
import org.plan.nutrition.server.configuration.RestServerConfiguration;
import org.plan.nutrition.server.data.dao.NutritiveDAO;
import org.plan.nutrition.server.data.entity.Nutritive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ActiveProfiles(profiles = ProfileDefinition.DEVELOPMENT)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RestServerConfiguration.class)
@WebAppConfiguration
public class ServerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private NutritiveDAO nutritiveDAO;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    private List<Nutritive> createTestData() {
        List<Nutritive> nutritives = new ArrayList<>();
        Nutritive chickenMeat = new Nutritive();
        chickenMeat.setName("chicken");
        chickenMeat.setEnergy(10);
        chickenMeat.setProtein(30);
        chickenMeat.setCarbohydrate(0);
        chickenMeat.setFat(0.1);
        nutritiveDAO.persist(chickenMeat);
        nutritives.add(chickenMeat);

        Nutritive rise = new Nutritive();
        rise.setName("rise");
        rise.setEnergy(30);
        rise.setProtein(2);
        rise.setCarbohydrate(80);
        rise.setFat(0);
        nutritiveDAO.persist(rise);
        nutritives.add(rise);

        return nutritives;
    }

    @Transactional
    @Test
    public void createNutritiveTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer();
        Nutritive chickenMeat = new Nutritive();
        chickenMeat.setName("chicken");
        chickenMeat.setEnergy(10);
        chickenMeat.setProtein(30);
        chickenMeat.setCarbohydrate(0);
        chickenMeat.setFat(0.1);

        String jsonString = writer.writeValueAsString(chickenMeat);
        MockHttpServletRequestBuilder request = post("/nutritives")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString);
        ResultActions resultActions = mvc.perform(request);
        MvcResult result = resultActions.andReturn();
        MockHttpServletResponse response = result.getResponse();
        String stringContent = response.getContentAsString();

        System.out.println("Result content:" + stringContent);
        ObjectReader reader = mapper.readerFor(Nutritive.class);
        Nutritive nutritive = reader.readValue(stringContent);

        System.out.println(nutritiveDAO.findByName(nutritive.getName()));
    }

    @Transactional
    @Test
    public void updateNutritiveTest() throws Exception {
        List<Nutritive> nutritives = createTestData();
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writer();

        Nutritive updatedNutritive = nutritives.get(0);
        updatedNutritive.setName("Update-test");

        String jsonString = writer.writeValueAsString(updatedNutritive);
        MockHttpServletRequestBuilder request = put("/nutritives?id=" + updatedNutritive.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString);
        ResultActions resultActions = mvc.perform(request);
        MvcResult result = resultActions.andReturn();
        MockHttpServletResponse response = result.getResponse();
        String stringContent = response.getContentAsString();

        System.out.println("Result content:" + stringContent);
        ObjectReader reader = mapper.readerFor(Nutritive.class);
        Nutritive nutritive = reader.readValue(stringContent);

        System.out.println(nutritiveDAO.findByName(nutritive.getName()));
    }

    @Transactional
    @Test
    public void getAllNutritivesJsonTest() throws Exception {
        createTestData();

        ResultActions resultActions = mvc.perform(get("/nutritives"));
        MvcResult result = resultActions.andReturn();
        String stringContent = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader reader = mapper.readerFor(Nutritive.class);
        Iterator<Nutritive> nutritiveIterator = reader.readValues(stringContent);

        System.out.println(stringContent);

        while (nutritiveIterator.hasNext()) {
            System.out.println(nutritiveIterator.next());
        }
    }

    @Transactional
    @Test
    public void getSingleNutritiveJsonTest() throws Exception {
        createTestData();

        ResultActions resultActions = mvc.perform(get("/nutritives?name=rise"));
        MvcResult result = resultActions.andReturn();
        String stringContent = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader reader = mapper.readerFor(Nutritive.class);
        Nutritive rise = reader.readValue(stringContent);

        System.out.println(stringContent);
        System.out.println(rise);
    }

    @Transactional
    @Test
    public void deleteNutritiveTest() throws Exception {
        createTestData();

        ResultActions resultActions = mvc.perform(get("/nutritives?name=rise"));
        MvcResult result = resultActions.andReturn();
        String stringContent = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        ObjectReader reader = mapper.readerFor(Nutritive.class);
        Nutritive rise = reader.readValue(stringContent);

        // do delete
        resultActions = mvc.perform(delete("/nutritives")
                .param("id", String.valueOf(rise.getId())));
        result = resultActions.andReturn();

        System.out.println(nutritiveDAO.findByName(rise.getName()));
    }
}
