package org.plan.nutrition.server.util;

import org.plan.nutrition.server.data.Column;
import org.springframework.stereotype.Component;

@Component
public class HqlBuilder {

    private static final String QUERY_FORMAT_STRING = "from %s";

    public String buildQueryForColumns(Class entityClass, Column... columns) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(String.format(QUERY_FORMAT_STRING, entityClass.getSimpleName()));

        if (columns.length >= 1) {
            queryBuilder.append(buildWhereClause(columns));
        }

        return queryBuilder.toString();
    }

    private String buildWhereClause(Column... columns) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(" where ");
        for (Column column : columns) {
            queryBuilder.append(column.getName()).append(" = :").append(column.getName());
        }
        return queryBuilder.toString();
    }

}
