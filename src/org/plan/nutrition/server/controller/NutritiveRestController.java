package org.plan.nutrition.server.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.plan.nutrition.server.data.dao.NutritiveDAO;
import org.plan.nutrition.server.data.entity.Nutritive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collection;

@RequestMapping("/")
@RestController
public class NutritiveRestController {

    private static final Log LOG = LogFactory.getLog(NutritiveRestController.class);

    @Autowired
    private NutritiveDAO nutritiveDAO;

    @Transactional
    @RequestMapping(method = RequestMethod.POST, path = "nutritives")
    public Nutritive createNutritive(@RequestBody Nutritive nutritive) {
        LOG.info(String.format("Got request createNutritive with params %s",
                nutritive));
        nutritiveDAO.persist(nutritive);

        return nutritive;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT, path = "nutritives", params = "id")
    public Nutritive updateNutritive(String id, @RequestBody Nutritive nutritive) {
        LOG.info(String.format("Got request updateNutritive with params %s",
                Arrays.asList(id, nutritive)));
        Nutritive persistedNutritive = nutritiveDAO.findById(Long.valueOf(id));
        persistedNutritive.setName(nutritive.getName());
        persistedNutritive.setEnergy(nutritive.getEnergy());
        persistedNutritive.setProtein(nutritive.getProtein());
        persistedNutritive.setCarbohydrate(nutritive.getCarbohydrate());
        persistedNutritive.setFat(nutritive.getFat());

        return nutritive;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, path = "nutritives")
    public Collection<Nutritive> getNutritives() {
        LOG.info(String.format("Got request getNutritives without params"));
        Collection<Nutritive> nutritiveCollection = nutritiveDAO.findAll();
        return nutritiveCollection;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, path = "nutritives", params = "name")
    public Nutritive getNutritiveByName(String name) {
        LOG.info(String.format("Got request getNutritiveByName with params %s", name));
        Nutritive nutritive = nutritiveDAO.findByName(name);
        return nutritive;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE, path = "nutritives", params = "id")
    public void deleteNutritive(String id) {
        LOG.info(String.format("Got request deleteNutritive with params %s", id));
        Nutritive nutritive = nutritiveDAO.findById(Long.valueOf(id));
        if (nutritive != null) {
            nutritiveDAO.remove(nutritive);
        }
    }
}
