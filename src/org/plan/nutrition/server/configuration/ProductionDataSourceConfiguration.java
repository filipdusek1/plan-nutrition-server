package org.plan.nutrition.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Profile(ProfileDefinition.PRODUCTION)
@PropertySource({"classpath:production-database.properties"})
@Configuration
public class ProductionDataSourceConfiguration {

}
