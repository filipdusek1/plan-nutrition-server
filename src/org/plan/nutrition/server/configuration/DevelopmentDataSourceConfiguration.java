package org.plan.nutrition.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Profile(ProfileDefinition.DEVELOPMENT)
@PropertySource({"classpath:development-database.properties"})
@Configuration
public class DevelopmentDataSourceConfiguration {

}
