package org.plan.nutrition.server.configuration;

public class ProfileDefinition {

    public static final String DEVELOPMENT = "development";

    public static final String PRODUCTION = "production";
}
