package org.plan.nutrition.server.data;

public class Column {

    private String name;
    private Object value;

    public Column(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
