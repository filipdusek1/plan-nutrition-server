package org.plan.nutrition.server.data.entity;

import javax.persistence.*;

@Entity
@Table(name = "NUTRITIVE")
public class Nutritive {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ENERGY")
    private int energy;

    @Column(name = "PROTEIN")
    private double protein;

    @Column(name = "CARBOHYDRATE")
    private double carbohydrate;

    @Column(name = "FAT")
    private double fat;

    public Nutritive() {
        super();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public String toString() {
        StringBuilder toStringBuilder = new StringBuilder(getClass().getSimpleName());
        toStringBuilder.append("{");
        toStringBuilder.append("id=").append(id).append(",");
        toStringBuilder.append("name=").append(name).append(",");
        toStringBuilder.append("energy=").append(energy).append(",");
        toStringBuilder.append("protein=").append(protein).append(",");
        toStringBuilder.append("carbohydrate=").append(carbohydrate).append(",");
        toStringBuilder.append("fat=").append(fat);
        toStringBuilder.append("}");
        return toStringBuilder.toString();
    }
}
