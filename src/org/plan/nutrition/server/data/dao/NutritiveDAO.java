package org.plan.nutrition.server.data.dao;

import org.plan.nutrition.server.data.Column;
import org.plan.nutrition.server.data.entity.Nutritive;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Repository
public class NutritiveDAO extends AbstractDAO<Nutritive> {

    @Transactional
    public Collection<Nutritive> findAll() {
        return findMultipleByColumnValues();
    }

    public Nutritive findByName(String name) {
        return super.findSingleByColumnValues(new Column("name", name));
    }
}
