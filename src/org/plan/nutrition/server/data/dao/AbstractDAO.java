package org.plan.nutrition.server.data.dao;

import org.plan.nutrition.server.data.Column;
import org.plan.nutrition.server.util.HqlBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

@Repository
public abstract class AbstractDAO<T> {

    private final Class<T> ENTITY_CLASS;
    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    protected HqlBuilder hqlBuilder;

    AbstractDAO() {
        super();
        Class clazz = this.getClass();
        Type type = clazz.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ENTITY_CLASS = (Class<T>) parameterizedType.getActualTypeArguments()[0];
        } else {
            throw new RuntimeException("Unable to initialize " + this.getClass());
        }
    }

    public T findById(long id) {
        return findSingleByColumnValues(new Column("id", id));
    }

    public void persist(T entity) {
        entityManager.persist(entity);
    }

    protected Collection<T> findMultipleByColumnValues(Column... columns) {
        TypedQuery<T> query = buildQueryForColumnValues(columns);
        return query.getResultList();
    }

    protected T findSingleByColumnValues(Column... columns) {
        TypedQuery<T> query = buildQueryForColumnValues(columns);
        try {
            return query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public void remove(T entity) {
        entityManager.remove(entity);
    }

    private TypedQuery<T> buildQueryForColumnValues(Column... columns) {
        String hqlQuery = hqlBuilder.buildQueryForColumns(ENTITY_CLASS, columns);
        TypedQuery<T> query = entityManager.createQuery(hqlQuery, ENTITY_CLASS);

        for (Column column : columns) {
            query.setParameter(column.getName(), column.getValue());
        }
        return query;
    }
}
